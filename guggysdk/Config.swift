//
//  Config.swift
//  guggysdk
//
//  Created by Shahar Yakir on 05/07/2016.
//  Copyright © 2016 Guggy. All rights reserved.
//

import Foundation

struct Config {
    
    static func textToGIFUrl() -> String {
        
        return "https://text2gif.guggy.com/v2/guggify"
        
    }
    
}
