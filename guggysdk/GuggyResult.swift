//
//  Result.swift
//  guggysdk
//
//  Created by Shahar Yakir on 05/07/2016.
//  Copyright © 2016 Guggy. All rights reserved.
//
import Foundation
import ObjectMapper

public class GuggyResult : Mappable {
    
    // MARK: Props    
    public var animated: [AnimatedMediaResult]?
    public var sticker: [StickerMediaResult]?
    public var reqId: String?
    
    required public init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        animated <- map["animated"]
        sticker  <- map["sticker"]
        reqId <- map["reqId"]
    }
    
}
