//
//  guggysdk.h
//  guggysdk
//
//  Created by Shahar Yakir on 05/07/2016.
//  Copyright © 2016 Guggy. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for guggysdk.
FOUNDATION_EXPORT double guggysdkVersionNumber;

//! Project version string for guggysdk.
FOUNDATION_EXPORT const unsigned char guggysdkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <guggysdk/PublicHeader.h>


