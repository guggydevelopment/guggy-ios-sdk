//
//  StickerResult.swift
//  guggysdk
//
//  Created by Shahar Yakir on 15/12/2016.
//  Copyright © 2016 Guggy. All rights reserved.
//

import Foundation
import ObjectMapper

public class StickerResult : Mappable {
    
    // MARK: Props
    public var hires:MediaContainer?
    public var original:MediaContainer?
    public var preview:MediaContainer?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        hires <- map["hires"]
        original <- map["original"]
        preview <- map["preview"]
    }
    
}
