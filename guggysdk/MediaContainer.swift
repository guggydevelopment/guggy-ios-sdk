//
//  MediaContainer.swift
//  guggysdk
//
//  Created by Shahar Yakir on 15/12/2016.
//  Copyright © 2016 Guggy. All rights reserved.
//

import Foundation
import ObjectMapper

public class MediaContainer : Mappable {
    
    // MARK: Props
    public var url:String?
    public var secureUrl:String?
    public var dimensions:Dimensions?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        url <- map["url"]
        secureUrl  <- map["secureUrl"]
        dimensions  <- map["dimensions"]
    }
    
}
