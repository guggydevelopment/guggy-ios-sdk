//
//  MP4Result.swift
//  guggysdk
//
//  Created by Shahar Yakir on 15/12/2016.
//  Copyright © 2016 Guggy. All rights reserved.
//

import Foundation
import ObjectMapper

public class MP4Result : Mappable {
    
    // MARK: Props
    public var original:MediaContainer?
    public var preview:MediaContainer?
    public var hires:MediaContainer?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        original <- map["original"]
        hires <- map["hires"]
        preview <- map["preview"]
    }
    
    
}
