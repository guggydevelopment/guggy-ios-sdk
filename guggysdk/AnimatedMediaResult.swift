//
//  AnimatedMediaResult.swift
//  guggysdk
//
//  Created by Shahar Yakir on 15/12/2016.
//  Copyright © 2016 Guggy. All rights reserved.
//

import Foundation
import ObjectMapper

public class AnimatedMediaResult : Mappable {
    
    // MARK: Props
    public var gif:GifResult?
    public var mp4:MP4Result?
    public var thumbnail:ThumbnailResult?
    public var contextId:Int?
    public var context:String?
    
    required public init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        gif <- map["gif"]
        mp4 <- map["mp4"]
        thumbnail <- map["thumbnail"]
        contextId <- map["contextId"]
        context <- map["context"]
    }
    
    
}
