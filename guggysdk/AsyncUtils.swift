//
//  AsyncUtils.swift
//  guggysdk
//
//  Created by Shahar Yakir on 22/08/2016.
//  Copyright © 2016 Guggy. All rights reserved.
//

import Foundation

class AsyncUtils {
    
    static func bg (_ block:@escaping ()->()){
        
        DispatchQueue.global().async(execute: block)
        
    }
    
    static func fg (_ block:@escaping ()->()){
        
        DispatchQueue.main.async(execute: block)
        
    }
    
}
