//
//  GuggyRequest.swift
//  guggysdk
//
//  Created by shahar on 4/6/17.
//  Copyright © 2017 Guggy. All rights reserved.
//

import Foundation

public class GuggyRequest {
    
    var userParams: [String:AnyObject] = [:]
    
}

public class GuggyRequestBuilder {
    
    var request = GuggyRequest()
    
    public init() {
        
        request.userParams["platform"] = "ios" as AnyObject
        request.userParams["userId"] = Guggy.userId as AnyObject
        
        if let lang = Guggy.lang {
         
            request.userParams["lang"] = lang as AnyObject
            
        }
        
    }
    
    /**
     Adds a sentence to the request
     */
    public func sentence(_ sentence:String) -> GuggyRequestBuilder {
        
        request.userParams["sentence"] = sentence as AnyObject?
        
        return self
        
    }
    
    /**
     Allows sending custom params
     */
    public func userParams(_ userParams:[String:AnyObject]) -> GuggyRequestBuilder {
        
        userParams.forEach { (k,v) in request.userParams[k] = v }
        
        return self
        
    }
    
    /**
     Directs the engine to extract inverted/cynical contexts
     */
    public func invertMeaning() -> GuggyRequestBuilder {
        
        request.userParams["invertMeaning"] = true as AnyObject?
        
        return self
        
    }
    
    /**
     Requests that no text will be attached to media
     */
    public func noText() -> GuggyRequestBuilder {
        
        request.userParams["noText"] = true as AnyObject?
        
        return self
        
    }
    
    /**
     Adds a hidden meaning, such that the background media will be chosen by it, but the sentence() text will appear on the media
     */
    public func hiddenMeaning(_ hiddenMeaning:String) -> GuggyRequestBuilder{
        
        request.userParams["hiddenMeaning"] = hiddenMeaning as AnyObject?
        
        return self
        
    }
    
    /**
     Allows sending two character ISO 639-1 language code that will direct Guggy to use a specific language when extracting context and trending content.
     */
    public func lang(_ lang:String) -> GuggyRequestBuilder {
        
        request.userParams["lang"] = lang as AnyObject
        
        return self
        
    }
    
    public func build() -> GuggyRequest{
    
        return request
    
    }
    
}
