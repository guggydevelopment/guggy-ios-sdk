//
//  Dimensions.swift
//  guggysdk
//
//  Created by Shahar Yakir on 15/12/2016.
//  Copyright © 2016 Guggy. All rights reserved.
//

import Foundation
import ObjectMapper

public class Dimensions : Mappable {
    
    // MARK: Props
    public var width:Int?
    public var height:Int?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        width <- map["width"]
        height <- map["height"]
    }
    
}
