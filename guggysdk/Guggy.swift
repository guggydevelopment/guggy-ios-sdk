//
//  guggy.swift
//  guggysdk
//
//  Created by Shahar Yakir on 05/07/2016.
//  Copyright © 2016 Guggy. All rights reserved.
//

import Foundation
import PromiseKit

open class Guggy {
    
    static private(set) public var initialized = false
    static var apiKey:String!
    public static var userId:String!
    static var lang:String?
    
    fileprivate static func generateOrRetrieveUserId()->String {
        
        let preferences = UserDefaults.standard
        let key = "guggyUserId"
        
        var userId = preferences.string(forKey: key)
        
        if userId == nil {
            
            userId = "\(apiKey!)_\(Utils.randomAlphaNumericString(length: 10))"
            preferences.set(userId, forKey: key)
            preferences.synchronize()
            
        }
        
        return userId!
        
    }
    
    open static func initialize(_ apiKey: String) throws {
        
        if !initialized {
            
            initialized = true
            
            DataAccess.initialize()
            
            Guggy.apiKey = apiKey
            
            if let primaryLang = UITextInputMode.activeInputModes.first?.primaryLanguage {
                
                lang = String(primaryLang.characters.split(separator: "-")[0])
                
                if lang == "en" {
                    
                    lang = "en_gg"
                    
                }
                
            }
            
            userId = generateOrRetrieveUserId()
            
        } else {
            
            throw GuggyErrors.alreadyInitialized
            
        }
        
    }
    
    open static func destroy() {
        
        initialized = false
        apiKey = nil
        userId = nil
        
    }
    
    open static func createGug(_ request:GuggyRequest, onComplete:@escaping (GuggyResult?) -> Void){
        
        let _ = DataAccess.createGug(request)
            .then { result -> Void in
                
                if let result = result {
                    
                    onComplete(result)
                    
                } else {
                    
                    onComplete(nil)
                    
                }
                
            }.recover{ err-> Void in
                
                onComplete(nil)
                
        }
        
    }
    
    open static func makeConnectRequest() {
        
        DataAccess.makeConnectRequest()
        
    }
    
}
