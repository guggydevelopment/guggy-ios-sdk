//
//  ThumbnailResult.swift
//  guggysdk
//
//  Created by Shahar Yakir on 15/12/2016.
//  Copyright © 2016 Guggy. All rights reserved.
//

import Foundation
import ObjectMapper

public class ThumbnailResult : Mappable {
    
    // MARK: Props
    public var original:MediaContainer?
    public var id:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        original <- map["original"]
        id <- map["id"]
    }
    
}
