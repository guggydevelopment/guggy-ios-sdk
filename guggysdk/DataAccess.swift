//
//  DataAccess.swift
//  guggysdk
//
//  Created by Shahar Yakir on 24/08/2016.
//  Copyright © 2016 Guggy. All rights reserved.
//

import Foundation
import Foundation
import PromiseKit

class DataAccess {
    
    static func createGug(_ request:GuggyRequest) -> Promise<GuggyResult?> {
                
        guard Guggy.initialized else {
            
            return Promise { fulfill, reject in reject(GuggyErrors.notInitialized) }
            
        }
       
        let headers = [
            "apiKey": (Guggy.apiKey as String)
        ]
        
        let measure = Measure("Guggy:createGug()")
        
        return NetworkUtils.request(Config.textToGIFUrl(), params: request.userParams , headers: headers)
            .then { (guggyResult) -> Promise<GuggyResult?> in
                
                measure.end()
              
                return Promise(value:guggyResult)
                
            }.recover{ (err)->Promise<GuggyResult?> in
                
                print(err)
                return Promise(value:nil)
                
        }
        
    }
    
    static func initialize() {
        
        NetworkUtils.initialize()
        makeConnectRequest()
        
    }
    
    public static func makeConnectRequest() {
        
        NetworkUtils.makeConnectRequest(Config.textToGIFUrl())
        
    }
    
}
