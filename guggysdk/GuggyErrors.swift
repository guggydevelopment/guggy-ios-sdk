//
//  GuggyErrors.swift
//  guggysdk
//
//  Created by Shahar Yakir on 05/07/2016.
//  Copyright © 2016 Guggy. All rights reserved.
//

import Foundation

public enum GuggyErrors: Error {
    
    case alreadyInitialized
    case notInitialized
    case emptyText
    case malformedResult
    
}
