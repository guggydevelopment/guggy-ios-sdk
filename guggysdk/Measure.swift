//
//  Measure.swift
//  guggysdk
//
//  Created by Shahar Yakir on 24/08/2016.
//  Copyright © 2016 Guggy. All rights reserved.
//

import Foundation

class Measure {
    
    let start = Date()
    var name:String!
    
    init(_ name:String) {
        
        self.name = name
        
    }
    
    func end() {
        
        print("\(name): \(Date().timeIntervalSince(start))")
        
    }
    
}
