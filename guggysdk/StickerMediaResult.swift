//
//  StickerMediaResult.swift
//  guggysdk
//
//  Created by Shahar Yakir on 15/12/2016.
//  Copyright © 2016 Guggy. All rights reserved.
//

import Foundation
import ObjectMapper

public class StickerMediaResult : Mappable {
    
    // MARK: Props
    public var webp:StickerResult?
    public var png:StickerResult?
    public var contextId:Int?
    public var context:String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        webp <- map["webp"]
        png <- map["png"]
        contextId <- map["contextId"]
        context <- map["context"]
    }
    
}
