//
//  NetworkUtils.swift
//  guggysdk
//
//  Created by Shahar Yakir on 23/08/2016.
//  Copyright © 2016 Guggy. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import PromiseKit

class NetworkUtils {
    
    private static var alamofireManager:Alamofire.SessionManager!
    private static let TIMEOUT = 7.0
    
    internal static func initialize(){
        
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForResource = TIMEOUT
        configuration.timeoutIntervalForRequest = TIMEOUT
        alamofireManager = Alamofire.SessionManager(configuration: configuration)
        
    }
    
    internal static func request(_ url: String, params: [String:AnyObject], headers: [String:String]) -> Promise<GuggyResult>  {
        
        return Promise { fulfill, reject in
            
            alamofireManager
                .request(url, method:.post, parameters: params, encoding: JSONEncoding(), headers: headers)
                .validate(statusCode: 200...299)
                .responseObject() { (dataResponse: DataResponse<GuggyResult>) in
                
                    if let result = dataResponse.result.value {
                        
                        fulfill(result)
                        
                    } else {
                        
                        reject(GuggyErrors.malformedResult)
                        
                    }
                    
                }
            
        }
        
    }
    
    internal static func makeConnectRequest(_ url:String) {
        
        alamofireManager
            .request(url, method: .connect, parameters: nil, encoding: JSONEncoding(), headers: nil)
        
        
    }
    
}
