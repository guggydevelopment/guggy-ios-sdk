//
//  guggysdkTests.swift
//  guggysdkTests
//
//  Created by Shahar Yakir on 05/07/2016.
//  Copyright © 2016 Guggy. All rights reserved.
//

@testable import guggysdk
import Quick
import Nimble

class guggysdkTests: QuickSpec {
    
    override func spec() {
        
        describe("guggysdk") {
            
            it("test") {
             
                Guggy.onType("koko", { _ in }, { _ in })
                
                expect(true).to(equal(true))
                
            }
            
        }
        
    }
    
}
            
//            
//            it("should throw a not initialized exception") {
//                
//                expect { try Guggy.createGug(
//                    "baboon",
//                    onSuccess:{ _ in },
//                    onError: { _ in }
//                )}.to(throwError())
//                
//            }
//            
//            it("should throw an already initialized exception") {
//                
//                try! Guggy.initialize("xsxnRCp0KcT4")
//                
//                expect { try Guggy.initialize("") }.to(throwError())
//                
//            }
//            
//            it("shouldn't do much") {
//                
//                GuggyRetrieveURLButton()
//                
//                expect(true).to(beTrue())
//                
//            }
//            
//            it("should throw an an empty text error") {
//            
//                expect { try Guggy.createGug(
//                    "",
//                    onSuccess:{ _ in },
//                    onError: { _ in }
//                )}.to(throwError())
//                
//            }
//            
//            it("should return a gif URL") {
//                
//                waitUntil(timeout:10) { done in
//                    
//                    try! Guggy.createGug(
//                        "baboon",
//                        onSuccess:{ result in
//                            
//                            expect(result.url).to(contain(".gif"))
//                            done()
//                            
//                        },
//                        onError: {error in
//                            fail(error)
//                            done()
//                        }
//                    )
//                    
//                }
//                
//            }
//            
//            it("should return a gif") {
//                
//                waitUntil(timeout:10) { done in
//                    
//                    try! DataAccess.createGugAndRetrieveFile(
//                        "baboon",
//                        onSuccess:{ result in
//                            
//                            expect(result.length).to(beGreaterThan(3000))
//                            done()
//                            
//                        },
//                        onError: {error in
//                            fail(error)
//                            done()
//                        }
//                    )
//                    
//                }
//                
//            }
//            
//            it("should insert a gif to clipboard") {
//                
//                waitUntil(timeout:10) { done in
//                    
//                    UIPasteboard.generalPasteboard().items.removeAll()
//                    
//                    let num = UIPasteboard.generalPasteboard().numberOfItems
//                    
//                    try! Guggy.createGugToClipboard(
//                        "baboon",
//                        onSuccess:{ result in
//                            
//                            expect(UIPasteboard.generalPasteboard().numberOfItems).to(beGreaterThan(num))
//                            done()
//                            
//                        },
//                        onError: {error in
//                            
//                            fail(error)
//                            done()
//                            
//                        }
//                    )
//                    
//                }
//                
//            }
            